.. {{cookiecutter.project_name}} documentation master file, created by
   sphinx-quickstart on Thu Aug 30 00:42:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{ cookiecutter.project_name }}'s documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/readme
   modules/features
   modules/architecture
   modules/contributing
   modules/issue_template
   modules/installation
   modules/usage
   modules/authors
   modules/changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
