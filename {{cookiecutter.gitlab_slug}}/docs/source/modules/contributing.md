## Contributing

#### Commit format

See [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)

#### Review process

When a new feature is published on remote, it should be reviewed by peers. It
might lead to some rework until it is considered as done.

The author is responsible for creating the code being reviewed.

The reviewer is responsible for examining the code and reporting the results to
the author.

Added commits during the review should not be squashed until the end of the
whole process, in order to keep the review history.

When all issues have been resolved, the reviewer gives his agreement, with
a `ship it!` comment, then you can finish the feature by squashing the potential
commits, pushing force your branch and merging it.

#### Finish new feature

After passing the review process and playing all the different tests you can
rebase your feature on develop, resolve conflicts if there is any and then
finish your task.

If there are more commits than you wish to push please squash them with a
rebase:

    $ git rebase -i <COMMIT HASH>
    $ git push origin {{ cookiecutter.project_code }} --force

If there is no conflict / commit to squash use next command with -r flag

Then open a merge request from feature branch to `develop` and assign it to
reviewers.
