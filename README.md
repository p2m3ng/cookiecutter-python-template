# Cookiecutter python template

My personal template for a Python package based on the `cookiecutter` tool.

* GitHub repo: [https://github.com/cookiecutter/cookiecutter](https://github.com/cookiecutter/cookiecutter)
* Python package template: [https://github.com/audreyr/cookiecutter-pypackage/](https://github.com/audreyr/cookiecutter-pypackage/)
* Documentation: [https://cookiecutter-pypackage.readthedocs.io/](https://cookiecutter-pypackage.readthedocs.io/)


